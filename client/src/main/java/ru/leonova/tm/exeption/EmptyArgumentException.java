package ru.leonova.tm.exeption;

public class EmptyArgumentException extends Exception {
    public EmptyArgumentException(){
        super("Argument is empty!");
    }
    public EmptyArgumentException(String message){
        super(message);
    }
}
