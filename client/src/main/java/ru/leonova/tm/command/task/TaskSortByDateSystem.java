package ru.leonova.tm.command.task;

import ru.leonova.tm.command.AbstractCommand;

import java.lang.Exception;

public class TaskSortByDateSystem extends AbstractCommand {
    @Override
    public String getName() {
        return "t-sort-system-date";
    }

    @Override
    public String getDescription() {
        return "Sorted tasks by system date";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
//        @NotNull final Session session = serviceLocator.getSessionEndpoint().getCurrentSession();
//        @NotNull final List<Task> taskList = serviceLocator.getTaskEndpoint().sortTasksBySystemDate(session);
//        System.out.println("\nAfter Sorting by start date:");
//        @NotNull final SimpleDateFormat format = new SimpleDateFormat();
//        for (@NotNull final Task task : taskList) {
//            System.out.println(task.getName() + " " + format.format(task.getDateSystem()));
//        }
    }
}
