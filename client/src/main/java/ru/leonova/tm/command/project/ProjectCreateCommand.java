package ru.leonova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.*;
import ru.leonova.tm.command.AbstractCommand;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.lang.Exception;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public final class ProjectCreateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "create-p";
    }

    @Override
    public String getDescription() {
        return "Create project";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception{
        final SessionDTO session = serviceLocator.getCurrentSession();
        if(session==null) throw new Exception("You are have not session");
        serviceLocator.getSessionEndpoint().valid(session);
        System.out.print("[CREATE PROJECT]\nINTER NAME: \n");
        @NotNull final String projectName = getScanner().nextLine();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(projectName);
        System.out.println("date start project:");
        @NotNull String dateStartProject = getScanner().nextLine();
        System.out.println("date end project:");
        @NotNull String dateEndProject = getScanner().nextLine();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = simpleDateFormat.parse(dateStartProject);
        GregorianCalendar gregorianCalendar = (GregorianCalendar)GregorianCalendar.getInstance();
        gregorianCalendar.setTime(date);
        XMLGregorianCalendar xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
        Date date2 = simpleDateFormat.parse(dateEndProject);
        GregorianCalendar gregorianCalendar2 = (GregorianCalendar)GregorianCalendar.getInstance();
        gregorianCalendar2.setTime(date2);
        Date date3 = new Date();
        GregorianCalendar gregorianCalendar3 = (GregorianCalendar)GregorianCalendar.getInstance();
        gregorianCalendar3.setTime(date3);
        XMLGregorianCalendar xmlGregorianCalendar2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar2);
        XMLGregorianCalendar xmlGregCalDateSystem =  DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar3);
        project.setDescription(getDescription());
        project.setStatus(Status.PLANNED);
        serviceLocator.getProjectEndpoint().createProject(session, project, xmlGregorianCalendar, xmlGregorianCalendar2, xmlGregCalDateSystem);
        System.out.println("Project created");
    }
}
