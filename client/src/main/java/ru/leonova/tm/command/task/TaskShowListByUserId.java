package ru.leonova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.*;
import ru.leonova.tm.command.system.AboutCommand;

import java.lang.Exception;
import java.text.SimpleDateFormat;
import java.util.Collection;

public class TaskShowListByUserId extends AboutCommand {
    @Override
    public String getName() {
        return "list-t-u";
    }

    @Override
    public String getDescription() {
        return "Show tasks list for current user";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        final SessionDTO session = serviceLocator.getCurrentSession();
        if(session==null) throw new Exception("You are have not session");
        System.out.println("[TASK LIST]");
        @NotNull final Collection<TaskDTO> taskCollection = serviceLocator.getTaskEndpoint().findAllTasksByUserId(session);
        if (taskCollection.isEmpty())return;
        @NotNull final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        int i = 0;
        for (@NotNull final TaskDTO task : taskCollection) {
            i++;
            System.out.println(i + ". PROJECT ID: " + task.getProjectId() + ",TASK ID: " + task.getTaskId() + ", TASK NAME: " + task.getName()+ ", Date start: " + format.format(task.getDateStart()) + ", Date end: " +  format.format(task.getDateEnd()));
        }
    }
}
