package ru.leonova.tm.command.data.json;

import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.Domain;
import ru.leonova.tm.api.endpoint.DomainDTO;
import ru.leonova.tm.api.endpoint.Session;
import ru.leonova.tm.api.endpoint.SessionDTO;
import ru.leonova.tm.command.AbstractCommand;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;

public class DomainJAXBSaveToJSONCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "jaxb-save-json";
    }

    @Override
    public String getDescription() {
        return "Data save to json file through JAXB";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.setProperty("javax.xml.bind.context.factory","org.eclipse.persistence.jaxb.JAXBContextFactory");
        final SessionDTO session = serviceLocator.getCurrentSession();
        if(session==null) throw new Exception("You are have not session");
        @NotNull final File file = new File("./data/domain.json");
        @NotNull final DomainDTO domain = new DomainDTO();
        serviceLocator.getDomainEndpoint().save(session, domain);
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, Boolean.TRUE);
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(domain, file);
        System.out.println("Saved to "+file.getName());
    }
}
