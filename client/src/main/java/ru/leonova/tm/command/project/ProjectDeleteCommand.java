package ru.leonova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.Project;
import ru.leonova.tm.api.endpoint.ProjectDTO;
import ru.leonova.tm.api.endpoint.Session;
import ru.leonova.tm.api.endpoint.SessionDTO;
import ru.leonova.tm.command.AbstractCommand;

import java.util.Collection;

public final class ProjectDeleteCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "del-p";
    }

    @Override
    public String getDescription() {
        return "Delete project with all his tasks";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception{
        final SessionDTO session = serviceLocator.getCurrentSession();
        if(session==null) throw new Exception("You are have not session");
        serviceLocator.getSessionEndpoint().valid(session);
        System.out.println("[DELETE PROJECT BY ID]\nList projects:");
        @NotNull final Collection<ProjectDTO> projectCollection = serviceLocator.getProjectEndpoint().findAllProjectsByUserId(session);
        int i = 0;
        for (@NotNull final ProjectDTO project : projectCollection) {
            i++;
            System.out.println(i + ". ID PROJECT: " + project.getProjectId() + ", NAME: " + project.getName());
        }
        System.out.println("Enter id project:");
        @NotNull final String projectId = getScanner().nextLine();
        serviceLocator.getProjectEndpoint().deleteProject(session, projectId);
//        serviceLocator.getTaskEndpoint().deleteTasksByIdProject(session, projectId);
        System.out.println("Project with his tasks are removed");
    }
}
