package ru.leonova.tm.command.data.xml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.Domain;
import ru.leonova.tm.api.endpoint.DomainDTO;
import ru.leonova.tm.api.endpoint.Session;
import ru.leonova.tm.api.endpoint.SessionDTO;
import ru.leonova.tm.command.AbstractCommand;

import java.io.File;

public class DomainJaksonLoadFromXMLCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "jackson-loadUserList-xml";
    }

    @Override
    public String getDescription() {
        return "Load data to xml file via jackson";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        final SessionDTO session = serviceLocator.getCurrentSession();
        @NotNull final File file = new File("./data/jackson.xml");
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final DomainDTO domainResult = objectMapper.readValue(file, DomainDTO.class);
        serviceLocator.getDomainEndpoint().load(session,domainResult);
        System.out.println("Data loadUserList from " + file.getPath());
        System.out.println(domainResult.getUserDTOList());
        System.out.println(domainResult.getProjectDTOList());
        System.out.println(domainResult.getTaskDTOList());
    }
}
