package ru.leonova.tm.command.data.xml;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.Domain;
import ru.leonova.tm.api.endpoint.DomainDTO;
import ru.leonova.tm.api.endpoint.Session;
import ru.leonova.tm.api.endpoint.SessionDTO;
import ru.leonova.tm.command.AbstractCommand;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileReader;

public class DomainJAXBLoadFromXMLCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "jaxb-loadUserList-xml";
    }

    @Override
    public String getDescription() {
        return "Load data to xml with jaxb";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        final SessionDTO session = serviceLocator.getCurrentSession();
        @NotNull final File file = new File("./data/domain.xml");
        @NotNull final FileReader reader = new FileReader (file);
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        @NotNull final DomainDTO domainResult = (DomainDTO) unmarshaller.unmarshal(reader);
        serviceLocator.getDomainEndpoint().load(session, domainResult);
        domainResult.getProjectDTOList().forEach(project -> System.out.println(project.getName()+" "+project.getUserId()));
        domainResult.getTaskDTOList().forEach(task -> System.out.println(task.getName()+" "+task.getUserId()));
        domainResult.getUserDTOList().forEach(user -> System.out.println(user.getRoleType()+" "+user.getUserId()));
    }
}
