package ru.leonova.tm.command.data.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.DomainDTO;
import ru.leonova.tm.api.endpoint.SessionDTO;
import ru.leonova.tm.command.AbstractCommand;

import java.io.File;

public class DomainJaksonLoadToJsonCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "jackson-loadUserList-json";
    }

    @Override
    public String getDescription() {
        return "Load data to json file via jackson";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        final SessionDTO session = serviceLocator.getCurrentSession();
        if(session==null) throw new Exception("You are have not session");
        @NotNull final DomainDTO domain = new DomainDTO();
        serviceLocator.getDomainEndpoint().save(session, domain);
        @NotNull final File file = new File("./data/jackson.json");
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final DomainDTO domainResult = mapper.readValue(file, DomainDTO.class);
        serviceLocator.getDomainEndpoint().load(session, domainResult);
        System.out.println("Data loadUserList from " + file.getPath());
        System.out.println(domainResult.getProjectDTOList());
        System.out.println(domainResult.getTaskDTOList());
        System.out.println(domainResult.getUserDTOList());
    }
}
