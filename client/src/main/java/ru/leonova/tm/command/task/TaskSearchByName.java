package ru.leonova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.Session;
import ru.leonova.tm.api.endpoint.SessionDTO;
import ru.leonova.tm.api.endpoint.Task;
import ru.leonova.tm.api.endpoint.TaskDTO;
import ru.leonova.tm.command.AbstractCommand;

public class TaskSearchByName extends AbstractCommand {
    @Override
    public String getName() {
        return "search-t-by-name";
    }

    @Override
    public String getDescription() {
        return "Search task by name";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        System.out.println(getDescription());
        final SessionDTO session = serviceLocator.getCurrentSession();
        if(session==null) throw new Exception("You are have not session");
        System.out.println("\nEnter name task:");
        @NotNull final String taskName = getScanner().nextLine();
        @NotNull final TaskDTO task = serviceLocator.getTaskEndpoint().findTaskByName(session, taskName);
        System.out.println(task.getName()+" "+task.getDateStart()+" "+task.getDateEnd());
    }
}
