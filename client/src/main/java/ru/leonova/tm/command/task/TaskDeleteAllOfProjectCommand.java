package ru.leonova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.Project;
import ru.leonova.tm.api.endpoint.ProjectDTO;
import ru.leonova.tm.api.endpoint.Session;
import ru.leonova.tm.api.endpoint.SessionDTO;
import ru.leonova.tm.command.AbstractCommand;

import java.util.Collection;

public final class TaskDeleteAllOfProjectCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "del-all-t-t-p";
    }

    @Override
    public String getDescription() {
        return "Delete all task of the selected project";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DELETE ALL TASKS OF THE SELECTED PROJECT]\n");
        final SessionDTO session = serviceLocator.getCurrentSession();
        if(session==null) throw new Exception("You are have not session");
        Collection<ProjectDTO> projectCollection = serviceLocator.getProjectEndpoint().findAllProjectsByUserId(session);
        int i=0;
        for (ProjectDTO project:projectCollection) {
            i++;
            System.out.println(i + ". ID PROJECT: " + project.getProjectId() + ", NAME: " + project.getName());
        }
        System.out.println("Select ID project: ");
        String projectId = getScanner().nextLine();
        serviceLocator.getTaskEndpoint().deleteTasksByIdProject(session, projectId);
        System.out.println("All tasks of this project are deleted");
    }
}
