package ru.leonova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.*;
import ru.leonova.tm.command.AbstractCommand;

import java.lang.Exception;
import java.util.List;

public final class UserUpdateRoleCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "up-role";
    }

    @Override
    public String getDescription() {
        return "Admin can edit role user to admin";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        final SessionDTO session = serviceLocator.getCurrentSession();
        if(session==null) throw new Exception("You are have not session");
        System.out.println("[" + getDescription().toUpperCase() + "]");
        System.out.println("[List user:]");
        @NotNull final List<UserDTO> userCollection = serviceLocator.getUserEndpoint().getCollectionUsers(session);
        int i = 0;
        for (UserDTO user : userCollection) {
            i++;
            System.out.println(i+". Login: " + user.getLogin() + ", Password: " + user.getPassword() + ", Id: " + user.getUserId() + ", Role type: " + user.getRoleType());
        }
        System.out.println("Enter id user");
        @NotNull final UserDTO user = serviceLocator.getUserEndpoint().getUserById(session);
        if (user == null) {
            System.out.println("User with this id not found");
        } else {
            user.setRoleType(RoleType.ADMIN);
            System.out.println("Role user was update to admin");
        }
    }
}
