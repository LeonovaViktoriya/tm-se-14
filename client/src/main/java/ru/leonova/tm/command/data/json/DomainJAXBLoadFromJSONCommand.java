package ru.leonova.tm.command.data.json;

import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.eclipse.persistence.jaxb.JAXBContextProperties;
import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.Domain;
import ru.leonova.tm.api.endpoint.DomainDTO;
import ru.leonova.tm.api.endpoint.Session;
import ru.leonova.tm.api.endpoint.SessionDTO;
import ru.leonova.tm.command.AbstractCommand;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class DomainJAXBLoadFromJSONCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "jaxb-loadUserList-json";
    }

    @Override
    public String getDescription() {
        return "Data loadUserList from json file through JAXB";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        @NotNull final File file = new File("./data/domain.json");
        final SessionDTO session = serviceLocator.getCurrentSession();
        if(session==null) throw new Exception("You are have not session");
        @NotNull final Map<String, Object> properties = new HashMap<>();
        properties.put(JAXBContextProperties.MEDIA_TYPE, "application/json");
        properties.put(JAXBContextProperties.JSON_INCLUDE_ROOT, Boolean.TRUE);
        @NotNull final JAXBContext jaxbContext = JAXBContextFactory.createContext(new Class[]{Domain.class}, properties);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @NotNull final DomainDTO domainResult = (DomainDTO)unmarshaller.unmarshal(file);
        serviceLocator.getDomainEndpoint().load(session, domainResult);
        System.out.println("Load data from " + file.getPath());
        System.out.println(domainResult.getUserDTOList());
        System.out.println(domainResult.getProjectDTOList());
        System.out.println(domainResult.getTaskDTOList());

    }
}
