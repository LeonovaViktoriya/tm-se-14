package ru.leonova.tm.api.enumerated;

public enum Status {

    PLANNED("planned"),
    INPROCESS("in-process") ,
    READY("ready");

    private String status;

    Status(String status) {
        this.status = status;
    }
    public String getStatus() {
        return status;
    }
}
