package ru.leonova.tm.utils;

import lombok.SneakyThrows;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import ru.leonova.tm.api.repository.IProjectRepository;
import ru.leonova.tm.api.repository.ISessionRepository;
import ru.leonova.tm.api.repository.ITaskRepository;
import ru.leonova.tm.api.repository.IUserRepository;

import javax.sql.DataSource;

public class SqlSessionFactoryClass {

    public SqlSessionFactoryClass() {
    }
    @SneakyThrows
    public static SqlSessionFactory getSqlSessionFactory() {

        final DataSource dataSource = new PooledDataSource("com.mysql.jdbc.Driver", "jdbc:mysql://127.0.0.1:3306/task-manager", "root", "root");

        final TransactionFactory transactionFactory = new JdbcTransactionFactory();

        final Environment environment = new Environment("development", transactionFactory, dataSource);

        Configuration configuration = new Configuration(environment);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ITaskRepository.class);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(ISessionRepository.class);

        return new SqlSessionFactoryBuilder().build(configuration);
    }

}
