package ru.leonova.tm.utils;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.dto.ProjectDTO;
import ru.leonova.tm.dto.SessionDTO;
import ru.leonova.tm.dto.TaskDTO;
import ru.leonova.tm.dto.UserDTO;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.Session;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.entity.User;

public class ConventorDTOUtil {

    public static UserDTO getUserDTO(@NotNull final User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setUserId(user.getUserId());
        userDTO.setLogin(user.getLogin());
        userDTO.setPassword(user.getPassword());
        userDTO.setRoleType(user.getRoleType());
        return userDTO;
    }

    public static User getUser(@NotNull final UserDTO userDTO) {
        User user = new User();
        user.setUserId(userDTO.getUserId());
        user.setLogin(userDTO.getLogin());
        user.setPassword(userDTO.getPassword());
        user.setRoleType(userDTO.getRoleType());
        return user;
    }

    public static Project getProject(@NotNull final ProjectDTO projectDTO) {
        Project project = new Project();
        project.setName(projectDTO.getName());
        project.setDateSystem(projectDTO.getDateSystem());
        User user = new User();
        user.setUserId(projectDTO.getUserId());
        project.setUser(user);
        project.setDateEnd(projectDTO.getDateEnd());
        project.setDateStart(projectDTO.getDateStart());
        project.setStatus(projectDTO.getStatus());
        project.setProjectId(projectDTO.getProjectId());
        project.setDescription(projectDTO.getDescription());
        project.setProjectId(projectDTO.getProjectId());
        return project;
    }

    public static Task getTask(@NotNull final TaskDTO taskDTO) {
        Task task = new Task();
        task.setName(taskDTO.getName());
        task.setDateSystem(taskDTO.getDateSystem());
        task.getUser().setUserId(taskDTO.getUserId());
        task.setDateEnd(taskDTO.getDateEnd());
        task.setDateStart(taskDTO.getDateStart());
        task.setStatus(taskDTO.getStatus());
        task.setDescription(taskDTO.getDescription());
        task.setTaskId(taskDTO.getTaskId());
        Project project = new Project();
        project.setProjectId(taskDTO.getProjectId());
        task.setProject(project);
        return task;
    }

    public static TaskDTO getTaskDTO(@NotNull final Task task) {
        TaskDTO taskDTO = new TaskDTO();
        taskDTO.setName(task.getName());
        taskDTO.setDateSystem(task.getDateSystem());
        taskDTO.setUserId(task.getUser().getUserId());
        taskDTO.setDateEnd(task.getDateEnd());
        taskDTO.setDateStart(task.getDateStart());
        taskDTO.setStatus(task.getStatus());
        taskDTO.setDescription(task.getDescription());
        taskDTO.setTaskId(task.getTaskId());
        taskDTO.setProjectId(task.getProject().getProjectId());
        return taskDTO;
    }

    public static ProjectDTO getProjectDTO(Project project){
        ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setUserId(project.getUser().getUserId());
        projectDTO.setName(project.getName());
        projectDTO.setDescription(project.getDescription());
        projectDTO.setDateSystem(project.getDateSystem());
        projectDTO.setDateStart(project.getDateStart());
        projectDTO.setDateEnd(project.getDateEnd());
        projectDTO.setStatus(project.getStatus());
        projectDTO.setUserId(project.getUser().getUserId());
        return projectDTO;
    }

    public static SessionDTO getSessionDTO(Session session){
        SessionDTO sessionDTO = new SessionDTO ();
        sessionDTO.setUserId(session.getUser().getUserId());
        sessionDTO.setSessionId(session.getSessionId());
        sessionDTO.setSignature(session.getSignature());
        sessionDTO.setTimestamp(session.getTimestamp());
        return sessionDTO;
    }

    public static Session getSession(SessionDTO sessionDTO){
        Session session = new Session();
        User user = new User();
        user.setUserId(sessionDTO.getUserId());
        session.setUser(user);
        session.setSessionId(sessionDTO.getSessionId());
        session.setSignature(sessionDTO.getSignature());
        session.setTimestamp(sessionDTO.getTimestamp());
        return session;
    }

}
