package ru.leonova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.IProjectEndpoint;
import ru.leonova.tm.api.service.IProjectService;
import ru.leonova.tm.api.service.ISessionService;
import ru.leonova.tm.dto.ProjectDTO;
import ru.leonova.tm.dto.SessionDTO;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.*;

@WebService(endpointInterface = "ru.leonova.tm.api.endpoint.IProjectEndpoint")
public class ProjectEndpoint implements IProjectEndpoint {

    private IProjectService projectService;
    private ISessionService sessionService;

    public ProjectEndpoint(IProjectService projectService, ISessionService sessionService) {
        this.projectService = projectService;
        this.sessionService = sessionService;
    }

    public static final String URL = "http://localhost:8080/ProjectEndpoint?wsdl";

    @WebMethod
    @Override
    public void createProject(@WebParam(name = "session") @NotNull SessionDTO session, @WebParam(name = "project") @NotNull ProjectDTO project,
                              @WebParam(name = "dateStart") @NotNull XMLGregorianCalendar dateStart,
                              @WebParam(name = "dateEnd") @NotNull XMLGregorianCalendar dateEnd,
                              @WebParam(name = "dateSystem") @NotNull XMLGregorianCalendar dateSystem
                              ) throws Exception {
        sessionService.valid(session);
        Date dateStartDate = dateStart.toGregorianCalendar().getTime();
        Date dateEndDate = dateEnd.toGregorianCalendar().getTime();
        Date dateSystemDate = dateSystem.toGregorianCalendar().getTime();
        project.setDateStart(dateStartDate);
        project.setDateEnd(dateEndDate);
        project.setDateSystem(dateSystemDate);
        project.setProjectId(UUID.randomUUID().toString());
        project.setUserId(session.getUserId());
        projectService.create(session.getUserId(), project);
    }

    @Override
    @WebMethod
    public void updateNameProject(@WebParam(name = "session") @NotNull final SessionDTO session, @WebParam(name = "projectId") @NotNull final String projectId,
                                  @WebParam(name = "name") @NotNull final String name) throws Exception{
        sessionService.valid(session);
        projectService.updateNameProject(session.getUserId(), projectId, name);
    }

    @Override
    @WebMethod
    public Collection<ProjectDTO> findAllProjectsByUserId(@WebParam(name = "session") @NotNull SessionDTO session) throws Exception{
        sessionService.valid(session);
        return projectService.findAllByUserId(session.getUserId());
    }

    @Override
    @WebMethod
    public void updateStatusProject(SessionDTO session, String projectId, String name) throws Exception {
        sessionService.valid(session);
        projectService.updateStatusProject(session.getUserId(), projectId, name);
    }

    @Override
    @WebMethod
    public void deleteProject(@WebParam(name = "session") @NotNull SessionDTO session, @WebParam(name = "projectId") @NotNull final String projectId) throws Exception{
        sessionService.valid(session);
        projectService.deleteProject(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    public void deleteAllProject(@WebParam(name = "session") @NotNull SessionDTO session) throws  Exception{
        sessionService.valid(session);
        projectService.deleteAllProject(session.getUserId());
    }

    @Override
    @WebMethod
    public List<ProjectDTO> sortProjectsBySystemDate(@WebParam(name = "session") @NotNull SessionDTO session) throws Exception{
        sessionService.valid(session);
        return projectService.sortProjectsBySystemDate(session.getUserId());
    }

    @Override
    @WebMethod
    public List<ProjectDTO> sortProjectsByStartDate(@WebParam(name = "session") @NotNull SessionDTO session) throws Exception{
        sessionService.valid(session);
        return projectService.sortProjectsByStartDate(session.getUserId());
    }

    @Override
    @WebMethod
    public List<ProjectDTO> sortProjectsByEndDate(@WebParam(name = "session") @NotNull SessionDTO session) throws Exception{
        sessionService.valid(session);
        return projectService.sortProjectsByEndDate(session.getUserId());
    }

    @Override
    @WebMethod
    public List<ProjectDTO> sortProjectsByStatus(@WebParam(name = "session") @NotNull SessionDTO session) throws Exception{
        sessionService.valid(session);
        return projectService.sortProjectsByStatus(session.getUserId());
    }

    @Override
    @WebMethod
    public ProjectDTO searchProjectByName(
            @WebParam(name = "session") @NotNull SessionDTO session,
            @WebParam(name = "projectName") @NotNull final String projectName
    ) throws Exception {
        sessionService.valid(session);
        return projectService.searchProjectByName(session.getUserId(), projectName);
    }
}
