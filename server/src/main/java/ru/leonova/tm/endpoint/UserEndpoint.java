package ru.leonova.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.IUserEndpoint;
import ru.leonova.tm.dto.SessionDTO;
import ru.leonova.tm.dto.UserDTO;
import ru.leonova.tm.entity.Session;
import ru.leonova.tm.api.service.ISessionService;
import ru.leonova.tm.api.service.IUserService;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.exeption.EmptyCollectionException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService(endpointInterface = "ru.leonova.tm.api.endpoint.IUserEndpoint")
public class UserEndpoint implements IUserEndpoint {

    private IUserService userService;
    private ISessionService sessionService;

    public UserEndpoint(IUserService userService, ISessionService sessionService) {
        this.userService = userService;
        this.sessionService = sessionService;
    }

    public static final String URL_USER = "http://localhost:8080/UserEndpoint?wsdl";

    @WebMethod
    @Override
    public void logOut(@WebParam(name = "session") @NotNull SessionDTO  session) throws Exception {
        sessionService.valid(session);
        sessionService.closeSession(session);
    }

    @WebMethod
    @Override
    public void addUser(@NotNull @WebParam(name = "login", partName = "login") final String login, @NotNull @WebParam(name = "password", partName = "password") final String password) throws Exception {
        UserDTO user = new UserDTO(login, password);
        userService.merge(user);
    }

    @Override
    public void loadUserList(@WebParam(name = "session") @NotNull SessionDTO session, @NotNull List<UserDTO>  list) throws Exception {
        sessionService.valid(session);
        userService.load(list);
    }

    @WebMethod
    @Override
    public Collection<UserDTO>  getCollectionUsers(@WebParam(name = "session") @NotNull SessionDTO  session) throws Exception {
        sessionService.valid(session);
        return userService.getCollection();
    }

    @WebMethod
    @Override
    public void deleteUser(@WebParam(name = "session") @NotNull SessionDTO  session) throws Exception {
        sessionService.valid(session);
        userService.removeUser(session.getUserId());
    }


    @WebMethod
    @Override
    @SneakyThrows
    public SessionDTO  authorizationUserAndOpenSession(@WebParam(name = "login") @NotNull final String login, @WebParam(name = "password") @NotNull final String password) throws Exception {
        if (login.isEmpty() || password.isEmpty()) throw new Exception();
        UserDTO user = userService.authorizationUser(login, password);
        if (user == null) throw new Exception("You are not authorized");
        return sessionService.openSession(login, password);
    }

    @WebMethod
    @Override
    public UserDTO getUserById(@WebParam(name = "session") @NotNull SessionDTO  session) throws Exception{
        sessionService.valid(session);
        if (session.getUserId().isEmpty()) throw new Exception();
        return userService.getById(session.getUserId());
    }

    @WebMethod
    @Override
    public void addAllUsers(@WebParam(name = "session") @NotNull SessionDTO  session, @WebParam(name = "userList") @NotNull final List<UserDTO>  users) throws Exception {
        sessionService.valid(session);
        if (users.isEmpty()) throw new EmptyCollectionException();
        userService.addAll(users);
    }

    @WebMethod
    @Override
    public void updateLoginUser(@WebParam(name = "session") @NotNull SessionDTO  session, @WebParam(name = "login") @NotNull String login) throws Exception {
        sessionService.valid(session);
        @NotNull final UserDTO user = userService.getById(session.getUserId());
        if (user==null) throw new Exception();
        userService.updateLogin(user,login);
    }

    @WebMethod
    @Override
    public void updatePasswordUser(@WebParam(name = "session") @NotNull SessionDTO  session, @WebParam(name = "password") @NotNull String password) throws Exception {
        sessionService.valid(session);
        @NotNull final UserDTO user = userService.getById(session.getUserId());
        if (user==null) throw new Exception();
        userService.updatePassword(user,password);
    }
}
