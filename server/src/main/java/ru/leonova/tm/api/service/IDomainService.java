package ru.leonova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.dto.DomainDTO;
import ru.leonova.tm.entity.Domain;

import java.sql.SQLException;

public interface IDomainService {
    void save(@NotNull DomainDTO domain) throws Exception;

    void load(@NotNull DomainDTO domain) throws Exception;

}
