package ru.leonova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.dto.DomainDTO;
import ru.leonova.tm.dto.SessionDTO;
import ru.leonova.tm.entity.Session;
import ru.leonova.tm.entity.Domain;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IDomainEndpoint {

    @WebMethod
    void save(@WebParam(name = "session") @NotNull SessionDTO session, @NotNull DomainDTO domain) throws java.lang.Exception;

    @WebMethod
    void load(@WebParam(name = "session") @NotNull SessionDTO session, @NotNull DomainDTO domain) throws java.lang.Exception;

}
