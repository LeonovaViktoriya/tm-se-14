package ru.leonova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;

import ru.leonova.tm.dto.ProjectDTO;
import ru.leonova.tm.dto.SessionDTO;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.Session;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Collection;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @WebMethod
    void createProject(@WebParam(name = "session") @NotNull SessionDTO session, @WebParam(name = "project") @NotNull ProjectDTO project,
                       @WebParam(name = "dateStart") XMLGregorianCalendar dateStart, @WebParam(name = "dateEnd") XMLGregorianCalendar dateEnd,
                       @WebParam(name = "dateSystem") @NotNull XMLGregorianCalendar dateSystem) throws java.lang.Exception;

    @WebMethod
    void updateNameProject(@WebParam(name = "session") @NotNull SessionDTO session, @WebParam(name = "projectId") @NotNull String projectId, @WebParam(name = "name") @NotNull String name) throws java.lang.Exception;

    @WebMethod
    Collection<ProjectDTO> findAllProjectsByUserId(@WebParam(name = "session") @NotNull SessionDTO session) throws java.lang.Exception;

    @WebMethod
    void updateStatusProject(SessionDTO session, String projectId, String name) throws java.lang.Exception;

    @WebMethod
    void deleteProject(@WebParam(name = "session") @NotNull SessionDTO session, @WebParam(name = "projectId") @NotNull String projectId) throws java.lang.Exception;

    @WebMethod
    void deleteAllProject(@WebParam(name = "session") @NotNull SessionDTO session) throws java.lang.Exception;

    @WebMethod
    List<ProjectDTO> sortProjectsBySystemDate(@WebParam(name = "session") @NotNull SessionDTO session) throws java.lang.Exception;

    @WebMethod
    List<ProjectDTO> sortProjectsByStartDate(@WebParam(name = "session") @NotNull SessionDTO session) throws java.lang.Exception;

    @WebMethod
    List<ProjectDTO> sortProjectsByEndDate(@WebParam(name = "session") @NotNull SessionDTO session) throws java.lang.Exception;

    @WebMethod
    List<ProjectDTO> sortProjectsByStatus(@WebParam(name = "session") @NotNull SessionDTO session) throws java.lang.Exception;

    @WebMethod
    ProjectDTO searchProjectByName(@WebParam(name = "session") @NotNull SessionDTO session, @WebParam(name = "projectName") @NotNull String projectName) throws java.lang.Exception;
}
