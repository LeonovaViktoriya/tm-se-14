package ru.leonova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.dto.SessionDTO;
import ru.leonova.tm.entity.Session;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ISessionEndpoint {
    @WebMethod
    boolean valid(@WebParam(name = "session", partName = "session") @NotNull SessionDTO session) throws java.lang.Exception;

    @WebMethod
    void closeSession(@WebParam(name = "session", partName = "session") @NotNull SessionDTO session) throws java.lang.Exception;

    @WebMethod
    SessionDTO openSession(@WebParam(name = "login", partName = "login") @NotNull String login, @WebParam(name = "password", partName = "password") @NotNull String password) throws java.lang.Exception;

}
