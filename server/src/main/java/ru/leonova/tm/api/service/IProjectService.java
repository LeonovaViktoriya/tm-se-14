package ru.leonova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.dto.ProjectDTO;
import ru.leonova.tm.entity.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectService {

    void load(@NotNull List<ProjectDTO> list) throws Exception;

    List<ProjectDTO> save() throws Exception;

    void create(@NotNull String userId, @NotNull ProjectDTO project) throws Exception;

    void updateNameProject(@NotNull String userId, @NotNull String projectId, @NotNull String name) throws Exception;

    Collection<ProjectDTO> findAllByUserId(@NotNull String userId) throws Exception;

    void deleteProject(@NotNull String userId, @NotNull String projectId) throws Exception;

    void deleteAllProject(String userId) throws Exception;

    List<ProjectDTO> sortProjectsBySystemDate(@NotNull String userId) throws Exception;

    List<ProjectDTO> sortProjectsByStartDate(@NotNull String userId) throws Exception;

    List<ProjectDTO> sortProjectsByEndDate(@NotNull String userId) throws Exception;

    List<ProjectDTO> sortProjectsByStatus(@NotNull String userId) throws Exception;

    ProjectDTO searchProjectByName(@NotNull String userId, @NotNull String projectName) throws Exception;

    ProjectDTO searchProjectByDescription(@NotNull String userId, @NotNull String description) throws Exception;

    void updateStatusProject(@NotNull String userId, @NotNull String projectId, @NotNull String name) throws Exception;
}
