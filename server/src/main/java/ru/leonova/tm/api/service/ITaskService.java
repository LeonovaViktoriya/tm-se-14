package ru.leonova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.dto.TaskDTO;
import ru.leonova.tm.entity.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskService {

    void create(@NotNull  String userId, @NotNull final TaskDTO task) throws Exception;

//    boolean isExist(@NotNull String userId, @NotNull String projectId) throws Exception;

    void load(@NotNull List<TaskDTO> list) throws Exception;

    List<TaskDTO> save();

    void updateTaskName(@NotNull String userId, @NotNull String taskId, @NotNull  String taskName) throws Exception;

    void deleteTaskByIdProject(@NotNull String userId, @NotNull String projectId) throws Exception;

    Collection<TaskDTO> findAllByUserId(@NotNull String userId) throws Exception;

    Collection<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId) throws Exception;

    void deleteTaskByIdUser(@NotNull String userId, @NotNull String taskId) throws Exception;

    void deleteAllTaskByUserId(@NotNull String userId) throws Exception;

    void merge(@NotNull TaskDTO task) throws Exception;

    void deleteAllTaskByProjectId(@NotNull String userId, @NotNull String projectId) throws Exception;

    TaskDTO findTaskByName(@NotNull String userId, @NotNull String taskName) throws Exception;

    List<TaskDTO> sortTasksByEndDate(@NotNull String userId) throws Exception;

    List<TaskDTO> sortTasksByStartDate(@NotNull String userId) throws Exception;

    List<TaskDTO> sortTasksBySystemDate(@NotNull String userId) throws Exception;

    List<TaskDTO> sortTasksByStatus(@NotNull String userId) throws Exception;

}
