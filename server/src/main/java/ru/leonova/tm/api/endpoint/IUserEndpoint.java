package ru.leonova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.dto.SessionDTO;
import ru.leonova.tm.dto.UserDTO;
import ru.leonova.tm.entity.Session;
import ru.leonova.tm.entity.User;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
public interface IUserEndpoint {

    @WebMethod void deleteUser(@WebParam(name = "session") @NotNull SessionDTO session) throws java.lang.Exception;

    void logOut(@WebParam(name = "session") @NotNull SessionDTO session) throws java.lang.Exception;

    @WebMethod
    void addUser(@WebParam(name = "l") @NotNull String l, @WebParam(name = "p") @NotNull String p) throws java.lang.Exception;

    @WebMethod
    void loadUserList(@WebParam(name = "session") @NotNull SessionDTO session, @WebParam(name = "list") @NotNull List<UserDTO> list) throws java.lang.Exception;

    @WebMethod
    Collection<UserDTO> getCollectionUsers(@WebParam(name = "session") @NotNull SessionDTO session) throws java.lang.Exception;

    @WebMethod
    SessionDTO authorizationUserAndOpenSession(@WebParam(name = "login") @NotNull String login, @WebParam(name = "password") @NotNull String password) throws java.lang.Exception;

    @WebMethod
    UserDTO getUserById(@WebParam(name = "session") @NotNull SessionDTO session) throws java.lang.Exception;

    @WebMethod
    void addAllUsers(@WebParam(name = "session") @NotNull SessionDTO session, @WebParam(name = "users") @NotNull List<UserDTO> users) throws java.lang.Exception;

    @WebMethod
    void updateLoginUser(@WebParam(name = "session") @NotNull SessionDTO session, @WebParam(name = "login") @NotNull String login) throws java.lang.Exception;

    @WebMethod
    void updatePasswordUser(@WebParam(name = "session") @NotNull SessionDTO session, @WebParam(name = "password") @NotNull String password) throws java.lang.Exception;
}
