package ru.leonova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.dto.UserDTO;
import ru.leonova.tm.entity.User;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public interface IUserService {


    void merge(@NotNull UserDTO user) throws Exception;

    void load(@NotNull List<UserDTO> list) throws Exception;

    void updateLogin(@NotNull UserDTO user, @NotNull String login) throws Exception;

    UserDTO authorizationUser(@NotNull String login, @NotNull String password) throws Exception;

    UserDTO getById(@NotNull String userId) throws Exception;

    List<UserDTO> save() throws SQLException;

    void removeUser(@NotNull String userId) throws Exception;

    Collection<UserDTO> getCollection() throws Exception;

    void adminRegistration(@NotNull String admin, @NotNull String admin1) throws Exception;

    String md5Apache(@NotNull String password) throws Exception;

    void addAll(@NotNull List<UserDTO> users) throws Exception;

    void updatePassword(@NotNull UserDTO user, @NotNull String password) throws Exception;
}
