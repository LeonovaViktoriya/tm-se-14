package ru.leonova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.User;

import java.sql.SQLException;
import java.util.Collection;

public interface IUserRepository {

//    @Insert("INSERT INTO app_user (id, login, password, role) VALUES (#{userId}, #{login}, #{password}, #{roleType})")
//    void persist(@NotNull User user) throws SQLException;
void persist(@NotNull User entity) throws SQLException;

    void updateUser(User user1);
//    @Update("UPDATE app_user SET login = #{login} WHERE id = #{id}")
//    void updateLogin(@NotNull @Param("id") String userId, @NotNull @Param("login") String login) throws SQLException;
//
//    @Select("SELECT * FROM app_user WHERE id = #{id}")
//    @Results(value = {
//            @Result(property = "userId", column = "id"),
//            @Result(property = "login", column = "login"),
//            @Result(property = "password", column = "password"),
//            @Result(property = "roleType", column = "role")})
//    User findOne(@NotNull String userId) throws Exception;
//
//    @Select("SELECT * FROM app_user WHERE login = #{login} AND password = #{password}")
//    @Results(value = {
//            @Result(property = "userId", column = "id"),
//            @Result(property = "login", column = "login"),
//            @Result(property = "password", column = "password"),
//            @Result(property = "roleType", column = "role")})
//    User findByLoginAndPassword(@NotNull @Param("login") String login, @NotNull @Param("password") String password) throws Exception;
//
//    @Select("SELECT * FROM app_user")
//    @Results(value = {
//            @Result(property = "userId", column = "id"),
//            @Result(property = "login", column = "login"),
//            @Result(property = "password", column = "password"),
//            @Result(property = "roleType", column = "role")})
//    Collection<User> findAll() throws SQLException;
//
//    @Delete("DELETE FROM app_user WHERE id = #{id}")
//    @Results(value = {
//            @Result(property = "userId", column = "id"),
//            @Result(property = "login", column = "login"),
//            @Result(property = "password", column = "password"),
//            @Result(property = "roleType", column = "role")})
//    void remove(@NotNull String userId) throws Exception;
//
//    @Delete("DELETE * FROM app_user")
//    @Results(value = {
//            @Result(property = "userId", column = "id"),
//            @Result(property = "login", column = "login"),
//            @Result(property = "password", column = "password"),
//            @Result(property = "roleType", column = "role")})
//    void removeAll() throws SQLException;
//
//    @Update("UPDATE app_user SET password = #{password} WHERE id = #{id}")
//    void updatePassword(@NotNull @Param("id") String userId, @NotNull @Param("password") String password) throws Exception;
}
