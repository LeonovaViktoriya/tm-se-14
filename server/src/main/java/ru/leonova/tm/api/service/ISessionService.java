package ru.leonova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.dto.SessionDTO;
import ru.leonova.tm.entity.Session;

import java.sql.SQLException;
import java.util.List;

public interface ISessionService {
    boolean valid(@NotNull SessionDTO session) throws Exception;

    void closeSession(@NotNull SessionDTO session) throws Exception;

    SessionDTO openSession(@NotNull String login, @NotNull String password) throws Exception;

    void removeAll(List<SessionDTO> sessions);

    SessionDTO getSessionByUserId(String userId) throws SQLException, Exception;
}
