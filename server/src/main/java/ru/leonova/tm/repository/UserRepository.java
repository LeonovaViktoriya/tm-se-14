package ru.leonova.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.repository.IUserRepository;
import ru.leonova.tm.entity.User;

import javax.persistence.EntityManager;
import java.util.List;

public class UserRepository {

    private EntityManager entityManager;

    public UserRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public User findOneByUserId(@NotNull final String userId){
        return entityManager.createQuery("FROM User u WHERE u.id = :userId", User.class).setParameter("userId", userId).getSingleResult();
    }

    public void persist(@NotNull final User user){
        entityManager.persist(user);
    }

    public void merge(@NotNull final User user){
        entityManager.merge(user);
    }

    public void remove(@NotNull final User user){
        entityManager.remove(user);
    }

    public List<User> findAll(){
        return entityManager.createQuery("FROM User u", User.class).getResultList();
    }

    public void updateUserLogin(@NotNull final String userId, @NotNull String login) {
        entityManager.createQuery("UPDATE User u SET u.login = :login WHERE u.id = :userId").
                setParameter("login", login).setParameter("userId",userId).setMaxResults(1).executeUpdate();
    }

    public User findByLogin(@NotNull final String login) {
        return entityManager.createQuery("FROM User u WHERE u.login = :login", User.class)
                .setParameter("login", login)
                .setMaxResults(1)
                .getSingleResult();
    }
}
