package ru.leonova.tm.repository;

import ru.leonova.tm.entity.Session;

import javax.persistence.EntityManager;
import java.util.List;

public class SessionRepository {

    private EntityManager entityManager;

    public SessionRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void remove(Session  session) {
        entityManager.remove(session);
    }

    public void persist(Session  session) {
        entityManager.persist(session);
    }

    public Session  findSessionByUserId(String userId) {
        return entityManager.createQuery("FROM Session s WHERE s.user.id = :userId", Session.class).setParameter("userId", userId).getSingleResult();
    }
}
