package ru.leonova.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Project;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Comparator;
import java.util.List;

public class ProjectRepository {

    private EntityManager entityManager;
    public ProjectRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public List<Project> findAllProjectsByUserId(@NotNull String userId) {
        return entityManager.createQuery("FROM Project p WHERE p.user.id =:userId ", Project.class).setParameter("userId", userId).getResultList();
    }

    @NotNull
    public List<Project> findAllProjects() {
        return entityManager.createQuery("FROM Project p", Project.class).getResultList();
    }

    @NotNull
    public Project findOneProjectByID(@NotNull String userId, @NotNull String projectId) {
        return entityManager.createQuery("FROM Project p WHERE p.user.id  =:userId AND p.id =:projectId", Project.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .setMaxResults(1)
                .getSingleResult();
    }

    @NotNull
    public Project findProjectByName(@NotNull String userId, @NotNull String projectName) {
        return entityManager.createQuery("FROM Project p WHERE p.user.id  =:userId AND p.name =:name", Project.class)
                .setParameter("userId", userId)
                .setParameter("name", projectName)
                .setMaxResults(1)
                .getSingleResult();
    }

    @NotNull
    public Project findProjectByDescription(@NotNull String userId, @NotNull String description) {
        return entityManager.createQuery("FROM Project p WHERE p.user.id  =:userId AND p.description =:description", Project.class)
                .setParameter("userId", userId)
                .setParameter("description", description)
                .setMaxResults(1)
                .getSingleResult();
    }

    @NotNull
    public List<Project> sortProjectsBySystemDate(@NotNull String userId) {
       List<Project> projectCollection = entityManager.createQuery("FROM Project p WHERE p.user.id =:userId", Project.class)
                .setParameter("userId", userId).getResultList();
        projectCollection.sort(Comparator.comparing(Project::getDateSystem));
        return projectCollection;
    }

    @NotNull
    public List<Project> sortProjectsByStartDate(@NotNull String userId) {
        List<Project> projectCollection = entityManager.createQuery("FROM Project p WHERE p.user.id  =:userId", Project.class)
                .setParameter("userId", userId).getResultList();
//        projectCollection.sort(Comparator.comparing(Project::getDateStart));
        return projectCollection;
    }

    @NotNull
    public List<Project> sortProjectsByEndDate(@NotNull String userId) {
        List<Project> projectCollection = entityManager.createQuery("FROM Project p WHERE p.user.id  =:userId", Project.class)
                .setParameter("userId", userId).getResultList();
        projectCollection.sort(Comparator.comparing(Project::getDateEnd));
        return projectCollection;
    }

    @NotNull
    public List<Project> sortProjectsByStatus(@NotNull String userId) {
        List<Project> projectCollection = entityManager.createQuery("FROM Project p WHERE p.user.id  =:userId", Project.class)
                .setParameter("userId", userId).getResultList();
        projectCollection.sort(Comparator.comparing(Project::getStatus));
        return projectCollection;
    }

    public void updateProject(@NotNull Project project){
        entityManager.refresh(project);
    }

    public void remove(@NotNull String projectId, @NotNull String userId) {
        Project project = entityManager.createQuery("FROM Project p WHERE p.id = :id AND p.user.id  = :userId", Project.class)
                .setParameter("id", projectId).setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult();
        entityManager.remove(project);
    }

    public void removeAllProjectsByUserId(@NotNull String userId) {
        for (Project Project : findAllProjectsByUserId(userId)) entityManager.remove(Project);
    }

    public void persist(@NotNull Project Project) {
        entityManager.persist(Project);
    }

    public void merge(@NotNull Project Project) {
        entityManager.merge(Project);
    }

    public void removeAll() {
        for (Project Project : findAllProjects())
            entityManager.remove(Project);
    }

}
