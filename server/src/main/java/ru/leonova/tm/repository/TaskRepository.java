package ru.leonova.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.Task;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public class TaskRepository {

    private EntityManager entityManager;

    public TaskRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public Task findOneById(@NotNull final String userId, @NotNull final String taskId) {
        return entityManager.createQuery("FROM Task t WHERE t.user.id = :userId AND t.id =:taskId", Task.class)
                .setParameter("userId", userId)
                .setParameter("taskId", taskId)
                .setMaxResults(1)
                .getSingleResult();
    }

    public void merge(@NotNull final Task task) {
        entityManager.merge(task);
    }

    public void persist(@NotNull final Task task) {
        entityManager.persist(task);
    }

    public List<Task> findAll() {
        return entityManager.createQuery("FROM Task t", Task.class).getResultList();
    }

    public void removeByIdProject(@NotNull final Task task) {
        entityManager.remove(task);
    }

    public void updateTaskName(@NotNull final Task task) {
        entityManager.refresh(task);
    }

    public List<Task> findAllByUserId(@NotNull final String userId) {
        return entityManager.createQuery("FROM Task t WHERE t.user.id=:userId", Task.class).setParameter("userId", userId).getResultList();
    }

    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return entityManager.createQuery("FROM Task t WHERE t.user.id=:userId AND t.project_id=:projectId", Task.class).
                setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    public void remove(@NotNull final Task task) {
        entityManager.remove(task);
    }

    public void removeAllTasksByUserId(@NotNull final String userId) {
        for (@NotNull final Task task: findAllByUserId(userId)) {
            entityManager.remove(task);
        }
    }

    public void removeAllTasksByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        for (@NotNull final Task task: findAllByProjectId(userId, projectId)) {
            entityManager.remove(task);
        }
    }

    public Task findOneByName(@NotNull final String userId, @NotNull final String taskName) {
        return entityManager.createQuery("FROM Task t WHERE t.user.id = :userId AND t.name =:taskName", Task.class)
                .setParameter("userId", userId)
                .setParameter("taskName", taskName)
                .setMaxResults(1)
                .getSingleResult();
    }

    public List<Task> sortByEndDate(@NotNull final String userId) {
        List<Task> taskList = entityManager.createQuery("FROM Task t WHERE t.user.id =:userId", Task.class)
                .setParameter("userId", userId).getResultList();
        taskList.sort(Comparator.comparing(Task::getDateEnd));
        return taskList;
    }

    public List<Task> sortByStartDate(@NotNull final String userId) {
        List<Task> taskList = entityManager.createQuery("FROM Task t WHERE t.user.id =:userId", Task.class)
                .setParameter("userId", userId).getResultList();
        taskList.sort(Comparator.comparing(Task::getDateStart));
        return taskList;
    }

    public List<Task> sortBySystemDate(@NotNull final String userId) {
        List<Task> taskList = entityManager.createQuery("FROM Task t WHERE t.user.id =:userId", Task.class)
                .setParameter("userId", userId).getResultList();
        taskList.sort(Comparator.comparing(Task::getDateSystem));
        return taskList;
    }

    public List<Task> sortByStatus(@NotNull final String userId) {
        List<Task> taskList = entityManager.createQuery("FROM Task t WHERE t.user.id =:userId", Task.class)
                .setParameter("userId", userId).getResultList();
        taskList.sort(Comparator.comparing(Task::getStatus));
        return taskList;
    }
}
