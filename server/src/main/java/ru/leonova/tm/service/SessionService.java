package ru.leonova.tm.service;

import lombok.Cleanup;
import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.ServiceLocator;
import ru.leonova.tm.api.service.ISessionService;
import ru.leonova.tm.dto.SessionDTO;
import ru.leonova.tm.dto.UserDTO;
import ru.leonova.tm.entity.Session;
import ru.leonova.tm.enumerated.RoleType;
import ru.leonova.tm.repository.SessionRepository;
import ru.leonova.tm.utils.ConventorDTOUtil;
import ru.leonova.tm.utils.HiberFactoryUtil;
import ru.leonova.tm.utils.SignatureUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;
import java.util.UUID;

public final class SessionService extends AbstractService<Session> implements ISessionService {

    private ServiceLocator serviceLocator;

    public SessionService(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public boolean valid(@NotNull final SessionDTO session) throws Exception {
        if (session.getSessionId().isEmpty() || session.getUserId().isEmpty() || session.getTimestamp().intValue() == 0 || session.getSignature().isEmpty() || session.getRoleType().getRole().isEmpty())
            throw new Exception("fields is empty");
        @NotNull final SessionDTO clone = session.clone();
        @NotNull final String sessionSignature = session.getSignature();
        clone.setSignature(null);
        final String cloneSignature = SignatureUtil.sign(clone);
        if (!sessionSignature.equals(cloneSignature)) throw new Exception("signatures not equals");
        @NotNull final long TimeStampCurrent = System.currentTimeMillis();
        if (TimeStampCurrent > session.getTimestamp() + (30 * 60 * 1000)) throw new Exception("SessionDTO time out");
        return true;
    }

    @Override
    public void closeSession(@NotNull final SessionDTO sessionDTO) {
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final SessionRepository sessionRepository = new SessionRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            Session session = ConventorDTOUtil.getSession(sessionDTO);
            sessionRepository.remove(session);
            entityManager.getTransaction().commit();
        }catch (Exception e){
            e.printStackTrace();
            entityManager.getTransaction().rollback();
        }
    }

    @Override
    public SessionDTO openSession(@NotNull final String login, @NotNull final String password) throws Exception {
        if (login.isEmpty() || password.isEmpty()) throw new Exception();
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final SessionRepository sessionRepository = new SessionRepository(entityManager);
        @NotNull final UserDTO user = serviceLocator.getUserService().authorizationUser(login, password);
        if (user == null) throw new Exception("This user not found");
        @NotNull final SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setSessionId(UUID.randomUUID().toString());
        sessionDTO.setUserId(user.getUserId());
        sessionDTO.setRoleType(user.getRoleType());
        sessionDTO.setTimestamp(System.currentTimeMillis());
        final String signature = SignatureUtil.sign(sessionDTO);
        if (signature == null) throw new Exception("Signature is null!");
        sessionDTO.setSignature(signature);
        Session session = ConventorDTOUtil.getSession(sessionDTO);
        try {
            entityManager.getTransaction().begin();
            sessionRepository.persist(session);
            entityManager.getTransaction().commit();
            return sessionDTO;
        } catch (Exception e) {
            e.printStackTrace();
            entityManager.getTransaction().rollback();
            return null;
        }
    }

    @Override
    public void removeAll(List<SessionDTO> sessionDTOList) {
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final SessionRepository sessionRepository = new SessionRepository(entityManager);
        entityManager.getTransaction().begin();
        for (SessionDTO sessionDTO:sessionDTOList) {
            Session session = ConventorDTOUtil.getSession(sessionDTO);
            sessionRepository.remove(session);
        }
        entityManager.getTransaction().commit();
    }

    @Override
    public SessionDTO getSessionByUserId(String userId) {
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final SessionRepository sessionRepository = new SessionRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            Session session = sessionRepository.findSessionByUserId(userId);
            entityManager.getTransaction().commit();
            return ConventorDTOUtil.getSessionDTO(session);
        } catch (Exception e) {
            e.printStackTrace();
            entityManager.getTransaction().rollback();
            return null;
        }
    }
}
