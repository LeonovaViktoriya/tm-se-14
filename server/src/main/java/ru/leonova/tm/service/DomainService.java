package ru.leonova.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.ServiceLocator;
import ru.leonova.tm.api.service.IDomainService;
import ru.leonova.tm.dto.DomainDTO;
import ru.leonova.tm.entity.Domain;

import java.sql.SQLException;

public class DomainService implements IDomainService {
    protected ServiceLocator serviceLocator;

    public DomainService(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void save(@NotNull final DomainDTO domainDTO) throws Exception {
        domainDTO.setProjectDTOList(serviceLocator.getProjectService().save());
        domainDTO.setTaskDTOList(serviceLocator.getTaskService().save());
        domainDTO.setUserDTOList((serviceLocator.getUserService().save()));
    }

    @Override
    public void load(@NotNull final DomainDTO domainDTO) throws Exception {
        serviceLocator.getProjectService().load(domainDTO.getProjectDTOList());
        serviceLocator.getTaskService().load(domainDTO.getTaskDTOList());
        serviceLocator.getUserService().load(domainDTO.getUserDTOList());
    }

}
