package ru.leonova.tm.service;

import lombok.Cleanup;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.repository.IUserRepository;
import ru.leonova.tm.api.service.IUserService;
import ru.leonova.tm.dto.UserDTO;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.enumerated.RoleType;
import ru.leonova.tm.repository.ProjectRepository;
import ru.leonova.tm.repository.UserRepository;
import ru.leonova.tm.utils.ConventorDTOUtil;
import ru.leonova.tm.utils.HiberFactoryUtil;
import ru.leonova.tm.utils.SqlSessionFactoryClass;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class UserService extends AbstractService<User> implements IUserService {

    @Override
    public void merge(@NotNull final UserDTO userDTO) {
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final User user = ConventorDTOUtil.getUser(userDTO);
            user.setRoleType(RoleType.USER);
            System.out.println(user.getLogin());
            userRepository.merge(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        }
    }

    @Override
    public void load(@NotNull final List<UserDTO> list) {
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            for (UserDTO userDTO : list) {
                @NotNull final User user = ConventorDTOUtil.getUser(userDTO);
                userRepository.persist(user);
            }
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        }
    }

    @Override
    public List<UserDTO> save() {
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            List<User> users = userRepository.findAll();
            entityManager.getTransaction().commit();
            List<UserDTO> usersDTO = new ArrayList<>();
            for (User user : users) {
                @NotNull final UserDTO userDTO = ConventorDTOUtil.getUserDTO(user);
                userRepository.persist(user);
                usersDTO.add(userDTO);
            }
            entityManager.getTransaction().commit();
            return usersDTO;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void removeUser(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception("User not found");
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final  User user = userRepository.findOneByUserId(userId);
            userRepository.remove(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        }
    }

    @Override
    public Collection<UserDTO> getCollection() {
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final Collection<User> users = userRepository.findAll();
            entityManager.getTransaction().commit();
            List<UserDTO> usersDTO = new ArrayList<>();
            for (User user : users) {
                @NotNull final UserDTO userDTO = ConventorDTOUtil.getUserDTO(user);
                userRepository.persist(user);
                usersDTO.add(userDTO);
            }
            entityManager.getTransaction().commit();
            return usersDTO;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void updateLogin(@NotNull final UserDTO user, @NotNull final String login) throws Exception {
        if (login.isEmpty() || user.getLogin().isEmpty()) throw new Exception("login is empty!");
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            userRepository.updateUserLogin(user.getUserId(), login);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        }
    }

    @Override
    public UserDTO authorizationUser(@NotNull final String login, @NotNull final String password) throws Exception {
        if (login.isEmpty() || password.isEmpty()) throw new Exception("login or password is empty!");
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final User user = userRepository.findByLogin(login);
            entityManager.getTransaction().commit();
            if(user.getPassword().equals(password)) return ConventorDTOUtil.getUserDTO(user);
            else return null;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public UserDTO getById(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception("Id user is empty!");
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final User user = userRepository.findOneByUserId(userId);
            entityManager.getTransaction().commit();
            return ConventorDTOUtil.getUserDTO(user);
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public String md5Apache(@NotNull final String password) throws Exception {
        if (password.isEmpty()) throw new Exception("password is empty!");
        return DigestUtils.md5Hex(password);
    }

    @Override
    public void addAll(@NotNull List<UserDTO> users) {
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final UserRepository userRepository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final List<User> userList = userRepository.findAll();
            entityManager.getTransaction().commit();
            for (User u: userList) {
                UserDTO userDTO = ConventorDTOUtil.getUserDTO(u);
                users.add(userDTO);
            }
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        }
    }

    @Override
    public void updatePassword(@NotNull UserDTO user, @NotNull String password) throws Exception {
        if (password.isEmpty()) throw new Exception("password is empty!");
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            User user1 = ConventorDTOUtil.getUser(user);
            user1.setPassword(password);
            userRepository.updateUser(user1);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            e.printStackTrace();
        }
    }

    @Override
    public void adminRegistration(@NotNull final String login, @NotNull String password) throws Exception {
        if (login.isEmpty() || password.isEmpty()) throw new Exception("login or password is empty!");
        @NotNull final SqlSessionFactory sqlSessionFactory = SqlSessionFactoryClass.getSqlSessionFactory();
        @Cleanup @NotNull final SqlSession sqlSession = sqlSessionFactory.openSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            @NotNull final User admin = new User("admin", md5Apache("admin"));
            admin.setRoleType(RoleType.ADMIN);
            userRepository.persist(admin);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
            e.printStackTrace();
        }
    }

}
