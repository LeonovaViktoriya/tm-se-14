package ru.leonova.tm.service;

import lombok.Cleanup;
import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.service.IProjectService;
import ru.leonova.tm.dto.ProjectDTO;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.enumerated.Status;
import ru.leonova.tm.repository.ProjectRepository;
import ru.leonova.tm.utils.ConventorDTOUtil;
import ru.leonova.tm.utils.HiberFactoryUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    @Override
    public void load(@NotNull final List<ProjectDTO> list) throws Exception {
        if (list.isEmpty()) throw new Exception();
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            list.forEach(project -> {
                projectRepository.persist(ConventorDTOUtil.getProject(project));
            });
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        }
    }

    @Override
    public List<ProjectDTO> save() {
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final Collection<Project> projects = projectRepository.findAllProjects();
            entityManager.getTransaction().commit();
            @NotNull final List<ProjectDTO> projectDTOList = new ArrayList<>();
            for (Project p : projects) {
                ProjectDTO projectDTO = ConventorDTOUtil.getProjectDTO(p);
                projectDTOList.add(projectDTO);
            }
            return projectDTOList;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void create(@NotNull final String userId, @NotNull final ProjectDTO projectDTO) throws Exception {
        @NotNull final String projectId = projectDTO.getProjectId();
        if (userId.isEmpty() || projectId.isEmpty()) throw new Exception("Id user or Id project is empty!");
        if (!projectDTO.getUserId().equals(userId)) throw new Exception("Access denied!");
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            User user = new User();
            user.setUserId(userId);
            @NotNull Project project = ConventorDTOUtil.getProject(projectDTO);
            projectRepository.merge(project);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        }
    }

    @Override
    public void updateNameProject(@NotNull final String userId, @NotNull final String projectId, @NotNull final String name) throws Exception {
        System.out.println(projectId);
        if (userId.isEmpty() || projectId.isEmpty() || name.isEmpty()) throw new Exception();
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final Project project = projectRepository.findOneProjectByID(userId, projectId);
            entityManager.getTransaction().commit();
            if (!project.getUser().getUserId().equals(userId)) throw new Exception("Access denied!");
            project.setName(name);
            projectRepository.updateProject(project);
            entityManager.getTransaction().commit();
        } catch (SQLException e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        }
    }

    @Override
    public Collection<ProjectDTO> findAllByUserId(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            Collection<Project> projectCollection = projectRepository.findAllProjectsByUserId(userId);
            entityManager.getTransaction().commit();
            @NotNull final List<ProjectDTO> projectDTOList = new ArrayList<>();
            for (Project p : projectCollection) {
                ProjectDTO projectDTO = ConventorDTOUtil.getProjectDTO(p);
                projectDTOList.add(projectDTO);
            }
            return projectDTOList;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void deleteProject(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        if (projectId.isEmpty() || userId.isEmpty()) throw new Exception();
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final Project project = projectRepository.findOneProjectByID(userId, projectId);
            entityManager.getTransaction().commit();
            if (project.getUser().getUserId().equals(userId)) throw new Exception("Access denied!");
            projectRepository.remove(userId, project.getProjectId());
            entityManager.getTransaction().commit();
        } catch (SQLException e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        }
    }

    @Override
    public void deleteAllProject(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            projectRepository.removeAllProjectsByUserId(userId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        }
    }

    @Override
    public List<ProjectDTO> sortProjectsBySystemDate(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final List<ProjectDTO> projectDTOList = new ArrayList<>();
            @NotNull final Collection<Project> projectCollection = projectRepository.sortProjectsBySystemDate(userId);
            entityManager.getTransaction().commit();
            for (Project p : projectCollection) {
                ProjectDTO projectDTO = ConventorDTOUtil.getProjectDTO(p);
                projectDTOList.add(projectDTO);
            }
            return projectDTOList;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<ProjectDTO> sortProjectsByStartDate(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final List<Project> projectCollection = projectRepository.sortProjectsByStartDate(userId);
            entityManager.getTransaction().commit();
            projectCollection.sort(Comparator.comparing(Project::getDateStart));
            @NotNull final List<ProjectDTO> projectDTOList = new ArrayList<>();
            for (Project p : projectCollection) {
                @NotNull final ProjectDTO projectDTO = ConventorDTOUtil.getProjectDTO(p);
                projectDTOList.add(projectDTO);
            }
            return projectDTOList;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<ProjectDTO> sortProjectsByEndDate(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final Collection<Project> projectCollection = projectRepository.sortProjectsByEndDate(userId);
            entityManager.getTransaction().commit();
            @NotNull final List<ProjectDTO> projectDTOList = new ArrayList<>();
            for (Project p : projectCollection) {
                ProjectDTO projectDTO = ConventorDTOUtil.getProjectDTO(p);
                projectDTOList.add(projectDTO);
            }
            return projectDTOList;
        } catch (Exception e) {
            e.printStackTrace();
            entityManager.getTransaction().rollback();
            return null;
        }
    }

    @Override
    public List<ProjectDTO> sortProjectsByStatus(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final Collection<Project> projectCollection = projectRepository.sortProjectsByStatus(userId);
            entityManager.getTransaction().commit();
            @NotNull final List<ProjectDTO> projectDTOList = new ArrayList<>();
            for (Project p : projectCollection) {
                ProjectDTO projectDTO = ConventorDTOUtil.getProjectDTO(p);
                projectDTOList.add(projectDTO);
            }
            return projectDTOList;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ProjectDTO searchProjectByName(@NotNull final String userId, @NotNull final String projectName) throws Exception {
        if (userId.isEmpty() || projectName.isEmpty()) throw new Exception();
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            Project project = projectRepository.findProjectByName(userId, projectName);
            entityManager.getTransaction().commit();
            return ConventorDTOUtil.getProjectDTO(project);
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ProjectDTO searchProjectByDescription(@NotNull final String userId, @NotNull final String description) throws Exception {
        if (userId.isEmpty() || description.isEmpty()) throw new Exception();
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final Project project = projectRepository.findProjectByDescription(description, userId);
            entityManager.getTransaction().commit();
            return ConventorDTOUtil.getProjectDTO(project);
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void updateStatusProject(@NotNull String userId, @NotNull String projectId, @NotNull String status) throws Exception {
        if (userId.isEmpty() || projectId.isEmpty() || status.isEmpty()) throw new Exception();
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final ProjectRepository projectRepository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final Project project = projectRepository.findOneProjectByID(userId, projectId);
            project.setStatus(Status.PLANNED);
            projectRepository.updateProject(project);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        }
    }
}
