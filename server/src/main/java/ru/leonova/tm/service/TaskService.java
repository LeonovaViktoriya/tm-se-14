package ru.leonova.tm.service;

import lombok.Cleanup;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.repository.ITaskRepository;
import ru.leonova.tm.api.service.ITaskService;
import ru.leonova.tm.dto.TaskDTO;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.repository.SessionRepository;
import ru.leonova.tm.repository.TaskRepository;
import ru.leonova.tm.utils.ConventorDTOUtil;
import ru.leonova.tm.utils.HiberFactoryUtil;
import ru.leonova.tm.utils.SqlSessionFactoryClass;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class TaskService extends AbstractService<TaskDTO> implements ITaskService {

    @Override
    public void create(@NotNull final String userId, @NotNull final TaskDTO taskDTO) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
        if (!taskDTO.getUserId().equals(userId)) throw new Exception();
        try {
            entityManager.getTransaction().begin();
            @NotNull final Task task = new Task();
            taskRepository.persist(task);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        }
    }

    @Override
    public void load(@NotNull List<TaskDTO> list) throws Exception {
        if (list.isEmpty()) throw new Exception();
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            list.forEach(taskDTO -> {
                @NotNull final Task task = ConventorDTOUtil.getTask(taskDTO);
                taskRepository.persist(task);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<TaskDTO> save() {
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final List<Task> taskList = taskRepository.findAll();
            entityManager.getTransaction().commit();
            @NotNull final  List<TaskDTO> taskDTOList = new ArrayList<>();
            for (@NotNull final Task task : taskList) {
                @NotNull final TaskDTO taskDTO = ConventorDTOUtil.getTaskDTO(task);
                taskDTOList.add(taskDTO);
            }
            return taskDTOList;
        } catch (Exception e) {
            e.printStackTrace();
            entityManager.getTransaction().rollback();
            return null;
        }
    }

    @Override
    public void updateTaskName(@NotNull String userId, @NotNull final String taskId, @NotNull final String taskName) throws Exception {
        if (taskName.isEmpty() || taskId.isEmpty() || taskId.equals(userId)) throw new Exception();
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final Task task = taskRepository.findOneById(userId, taskId);
            task.setName(taskName);
            taskRepository.updateTaskName(task);
            entityManager.getTransaction().commit();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        }
    }

    @Override
    public void deleteTaskByIdProject(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        if (projectId.isEmpty() || userId.isEmpty()) throw new Exception();
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final Task task = taskRepository.findOneById(userId, projectId);
            taskRepository.removeByIdProject(task);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        }
    }

    @Override
    public Collection<TaskDTO> findAllByUserId(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final List<Task> taskList = taskRepository.findAllByUserId(userId);
            entityManager.getTransaction().commit();
            @NotNull final List<TaskDTO> taskDTOList = new ArrayList<>();
            for (@NotNull final Task task : taskList) {
                @NotNull final TaskDTO taskDTO = ConventorDTOUtil.getTaskDTO(task);
                taskDTOList.add(taskDTO);
            }
            return taskDTOList;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Collection<TaskDTO> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        if (userId.isEmpty() || projectId.isEmpty()) throw new Exception();
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final List<Task> taskList = taskRepository.findAllByProjectId(userId, projectId);
            entityManager.getTransaction().commit();
            @NotNull final List<TaskDTO> taskDTOList = new ArrayList<>();
            for (@NotNull final Task task : taskList) {
                @NotNull final TaskDTO taskDTO = ConventorDTOUtil.getTaskDTO(task);
                taskDTOList.add(taskDTO);
            }
            return taskDTOList;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
            return null;
        }
    }


    @Override
    public void deleteTaskByIdUser(@NotNull final String userId, @NotNull final String taskId) throws Exception {
        if (taskId.isEmpty() || userId.isEmpty()) throw new Exception();
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final Task task = taskRepository.findOneById(userId,taskId);
            taskRepository.remove(task);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        }
    }


    @Override
    public void deleteAllTaskByUserId(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.removeAllTasksByUserId(userId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        }
    }

    @Override
    public void merge(@NotNull final TaskDTO taskDTO) throws Exception {
        if (taskDTO.getProjectId() == null || taskDTO.getProjectId().isEmpty()) throw new Exception();
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final Task task = ConventorDTOUtil.getTask(taskDTO);
            taskRepository.merge(task);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        }
    }

    @Override
    public void deleteAllTaskByProjectId(@NotNull String userId, @NotNull final String projectId) throws Exception {
        if (userId.isEmpty() || projectId.isEmpty()) throw new Exception();
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.removeAllTasksByProjectId(userId, projectId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        }
    }

    @Override
    public TaskDTO findTaskByName(@NotNull final String userId, @NotNull final String taskName) throws Exception {
        if (userId.isEmpty() || taskName.isEmpty()) throw new Exception();
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final Task task = taskRepository.findOneByName(userId, taskName);
            @NotNull final TaskDTO taskDTO = ConventorDTOUtil.getTaskDTO(task);
            entityManager.getTransaction().commit();
            return taskDTO;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<TaskDTO> sortTasksByEndDate(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final List<Task> taskList = taskRepository.sortByEndDate(userId);
            entityManager.getTransaction().commit();
            @NotNull final List<TaskDTO> taskDTOList = new ArrayList<>();
            for (@NotNull final Task task : taskList) {
                @NotNull final TaskDTO taskDTO = ConventorDTOUtil.getTaskDTO(task);
                taskDTOList.add(taskDTO);
            }
            return taskDTOList;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<TaskDTO> sortTasksByStartDate(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final List<Task> taskList = taskRepository.sortByStartDate(userId);
            entityManager.getTransaction().commit();
            @NotNull final List<TaskDTO> taskDTOList = new ArrayList<>();
            for (@NotNull final Task task : taskList) {
                @NotNull final TaskDTO taskDTO = ConventorDTOUtil.getTaskDTO(task);
                taskDTOList.add(taskDTO);
            }
            return taskDTOList;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<TaskDTO> sortTasksBySystemDate(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            @NotNull final List<Task> taskList = taskRepository.sortBySystemDate(userId);
            entityManager.getTransaction().commit();
            @NotNull final List<TaskDTO> taskDTOList = new ArrayList<>();
            for (@NotNull final Task task : taskList) {
                @NotNull final TaskDTO taskDTO = ConventorDTOUtil.getTaskDTO(task);
                taskDTOList.add(taskDTO);
            }
            return taskDTOList;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<TaskDTO> sortTasksByStatus(String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final EntityManagerFactory entityManagerFactory = HiberFactoryUtil.factory();
        @Cleanup @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final TaskRepository taskRepository = new TaskRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final List<Task> taskList = taskRepository.sortByStatus(userId);
            entityManager.getTransaction().commit();
            @NotNull final List<TaskDTO> taskDTOList = new ArrayList<>();
            for (@NotNull final Task task : taskList) {
                @NotNull final TaskDTO taskDTO = ConventorDTOUtil.getTaskDTO(task);
                taskDTOList.add(taskDTO);
            }
            return taskDTOList;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
            return null;
        }
    }

}
