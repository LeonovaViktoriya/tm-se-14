package ru.leonova.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.leonova.tm.enumerated.Status;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@Entity
public final class ProjectDTO implements Serializable {

    private  String name;
    @Id
    private String projectId;
    private String description;
    private Date dateSystem;
    private Date dateStart;
    private Date dateEnd;
    private String userId;
    private Status status;

    public ProjectDTO(String name, String userId) {
        this.name = name;
        this.userId = userId;
        projectId = UUID.randomUUID().toString();
    }
    public ProjectDTO(String name, String userId, Date dateStart, Date dateEnd) {
        this.name = name;
        this.userId = userId;
        projectId = UUID.randomUUID().toString();
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
    }
}
