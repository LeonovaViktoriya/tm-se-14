package ru.leonova.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.leonova.tm.enumerated.RoleType;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class UserDTO implements Serializable {

    private String userId;
    private String login;
    private String password;
    private RoleType roleType;

    public UserDTO(String login, String password){
        this.login = login;
        this.password = password;
        this.userId = UUID.randomUUID().toString();
    }
}
