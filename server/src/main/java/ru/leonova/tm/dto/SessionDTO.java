package ru.leonova.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.enumerated.RoleType;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class SessionDTO implements Cloneable, Serializable {

    private String sessionId;
    private String userId;
    private RoleType roleType;
    private Long timestamp;
    private String signature;

    @Override
    public SessionDTO clone() throws CloneNotSupportedException {
        return (SessionDTO) super.clone();
    }
}
